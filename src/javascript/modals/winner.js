import { showModal}  from "./modal";
import {createElement} from "../helpers/domHelper";
import { createImage, createName } from "../fighterView";


export  function showWinnerModal(fighter) {
    const { name, source } = fighter;
    const title = 'Winner!';
    
    const nameElement = createName(name);
    nameElement.innerText = `Name: ${name}\n`;
    
    const imageElement = createImage(source);
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const bodyElement = createElement({ tagName: 'div', className: 'fighter' });

    fighterDetails.append(nameElement, imageElement);
    bodyElement.append(fighterDetails);

    showModal({title, bodyElement});
}