import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

export function createFighterDetails(fighter) {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const healtElement = createElement({ tagName: 'span', className: 'fighter-healt' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src: source }});

  nameElement.innerText = `Name: ${name}\n`;
  healtElement.innerText = `Health: ${health}\n`;
  attackElement.innerText = `Attack: ${attack}\n`;
  defenseElement.innerText = `Defense: ${defense}\n`;
  imgElement.innerText = source;
  fighterDetails.append(nameElement, healtElement, attackElement, defenseElement, imgElement);
  return fighterDetails;
}
