export function fight(firstFighter, secondFighter) {
    firstFighter = Object.create(firstFighter);
    secondFighter = Object.create(secondFighter);
    
    console.log(firstFighter);
    console.log(secondFighter);
    
  let flag = true;
  while (firstFighter.health >= 0 && secondFighter.health >= 0) {
    if (flag) {
      console.error("FIRST HIT");
      console.log("before damage secondFighter");
      console.log(secondFighter.health);
      
      secondFighter.health -= getDamage(firstFighter, secondFighter);
      
      console.log("after damage secondFighter");
      console.log(secondFighter.health);
      
      flag = false;
    } else {
      console.error("SECOND HIT");
      console.log("before damage firstFighter");
      console.log(firstFighter.health);
      
      firstFighter.health -= getDamage(secondFighter, firstFighter);  
      
      console.log("before damage firstFighter");
      console.log(firstFighter.health);
      
        flag = true;
    }
  }
  console.error("Who is winner?");
  console.error(firstFighter);
  console.error(secondFighter);
  console.error("Winner is...");
  console.error(secondFighter.health <= 0 ? firstFighter : secondFighter);
      
  return firstFighter.health > secondFighter.health ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  damage = damage >= 0 ? damage : 0;
  return damage;
}

const criticalHitChance = () => Math.random() + 1;
const dodgeChance = () => Math.random() + 1;

export function getHitPower(fighter) {
  return fighter.attack * criticalHitChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * dodgeChance();
}
